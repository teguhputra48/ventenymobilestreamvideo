import 'package:get/get.dart';
import 'package:vidio_stream/modules/stream/stream.dart';
import 'package:vidio_stream/screens/stream.dart';


class Routes {
  static const String root = '/';
}

final List<GetPage> routes = [
  GetPage(name: Routes.root, page: () => Stream(Get.find()), binding: StreamBindings()),
];