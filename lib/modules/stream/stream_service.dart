import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:vidio_stream/models/Stream.dart';

class StreamService {
  Future<ListStream?> streamList() async {
    final String response = await rootBundle.loadString('./assets/json/data_json.json');
    final data = await json.decode(response);
    try {
      ListStream dataJson;
      dataJson = ListStream.fromJson(data);
      return dataJson;
    } catch (e) {
      print("streamList: "+e.toString());
      return null;
    }
  }
}