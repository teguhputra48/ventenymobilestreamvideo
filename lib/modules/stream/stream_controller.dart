import 'package:flutter/material.dart';
import 'package:flutter_meedu_videoplayer/meedu_player.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:vidio_stream/config/colors.dart';
import 'package:vidio_stream/models/Stream.dart';

import 'package:vidio_stream/modules/stream/stream.dart';

import '../../utils/api_request_status.dart';
import '../../utils/functions.dart';

class StreamController extends GetxController {

  final StreamService _service;
  final meeduPlayerController = MeeduPlayerController(
      controlsStyle: ControlsStyle.primary,
      colorTheme: ColorsValue.primarySwatch,
      loadingWidget: SpinKitWave(
        color: Theme.of(Get.context!).colorScheme.secondary,
      )
  );

  StreamController(this._service);
  var apiRequestStatus = APIRequestStatus.loading.obs;
  var isShowPreview = false.obs;
  var listVideo = <Results>[].obs;

  @override
  Future<void> onInit() async {
    loadData();
    super.onInit();
  }

  void setVideo(String url){
    isShowPreview.value = true;
    meeduPlayerController.setDataSource(
      DataSource(
        type: DataSourceType.network,
        source: url,
      ),
      autoplay: true,
    );
  }

  @override
  void onClose(){
    meeduPlayerController.dispose();
    super.onClose();
  }

  Future<void> refreshData() async {
    isShowPreview.value = false;
    apiRequestStatus.value = APIRequestStatus.loading;
    await Future.delayed(const Duration(seconds: 2));
    apiRequestStatus.value = APIRequestStatus.loaded;
  }

  Future<void> loadData() async {
    apiRequestStatus.value = APIRequestStatus.loading;
    await Future.delayed(const Duration(seconds: 2));
    try {
      ListStream? listStream = await _service.streamList();
      if(listStream != null) {
        listVideo.value = listStream.results!;
      }
      apiRequestStatus.value = APIRequestStatus.loaded;
    } catch (e) {
      checkError(e);
    }
  }

  void checkError(e) {
    if (Functions.checkConnectionError(e)) {
      apiRequestStatus.value = APIRequestStatus.connectionError;
    } else {
      apiRequestStatus.value = APIRequestStatus.error;
    }
  }

}