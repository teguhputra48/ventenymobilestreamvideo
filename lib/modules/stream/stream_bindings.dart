import 'package:get/get.dart';

import 'package:vidio_stream/modules/stream/stream.dart';

class StreamBindings implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => StreamController(StreamService()));
  }
}