import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:vidio_stream/config/config.dart';
import 'package:vidio_stream/utils/utils.dart';

ThemeData appTheme = createTheme(
  brightness: Brightness.light,
  systemOverlayStyle: SystemUiOverlayStyle.dark,
  primarySwatch: ColorsValue.primarySwatch,
  background: ColorsValue.backgroundColor,
  primaryText: ColorsValue.textColor,
  secondaryText: ColorsValue.secondaryTextColor,
  accentColor: ColorsValue.accentColor,
  divider: ColorsValue.dividerColor,
  buttonBackground: ColorsValue.darkPrimaryColor,
  buttonText: ColorsValue.secondaryTextColor,
  disabled: ColorsValue.secondaryTextColor,
  error: Colors.red
);