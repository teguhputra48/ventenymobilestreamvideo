import 'package:colour/colour.dart';
import 'package:flutter/material.dart';

class ColorsValue {
  static MaterialColor primarySwatch = Colors.lime;
  static Color darkPrimaryColor = const Color(0xffAFB42B);
  static Color backgroundColor = const Color(0xffFFFFFF);
  static Color lightPrimaryColor = const Color(0xffF0F4C3);
  static Color textColor = const Color(0xff212121);
  static Color accentColor = const Color(0xffCDDC39);
  static Color secondaryTextColor = const Color(0xff757575);
  static Color dividerColor = const Color(0xffBDBDBD);

}