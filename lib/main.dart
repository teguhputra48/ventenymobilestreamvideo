import 'package:flutter/material.dart';

import 'package:vidio_stream/app.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const App());
}