
import 'package:flutter/material.dart';
import 'package:flutter_meedu_videoplayer/meedu_player.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:vidio_stream/config/assets.dart';
import 'package:vidio_stream/config/colors.dart';

import 'package:vidio_stream/modules/stream/stream.dart';
import 'package:vidio_stream/utils/functions.dart';

import '../widgets/body_builder.dart';

class Stream extends StatelessWidget {
  final StreamController stream;

  const Stream(this.stream, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Obx(
        () =>
          BodyBuilder(
            apiRequestStatus: stream.apiRequestStatus.value,
            child: _buildBodyList(),
            reload: () => {
              stream.refreshData()
            },
          )
      ),
    );
  }

  Widget _buildBodyList() {
    return RefreshIndicator(
      onRefresh: () => stream.refreshData(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Visibility(child: AspectRatio(
            aspectRatio: 16 / 9,
            child: MeeduVideoPlayer(
              controller: stream.meeduPlayerController,
            ),
          ),
            visible: stream.isShowPreview.value,
          ),
          Gap(10.h),
          Expanded(child: ListView.builder(
              physics: const AlwaysScrollableScrollPhysics(),
              itemCount: stream.listVideo.length,
              itemBuilder: (BuildContext context, int index) {
                var dataView = stream.listVideo[index];
                return GestureDetector(
                  child: Card(
                    elevation: 10,
                    color: ColorsValue.backgroundColor,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 10.h),
                      width: Get.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Center(
                            child: SvgPicture.asset(
                              AssetsValue().playButton,
                              color: const Color(0xffFF8E15),
                              cacheColorFilter: true,
                              width: 20.w,
                              height: 20.h,
                            ),
                          ),
                          Gap(10.w),
                          Expanded(child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                dataView.trackName.toString(),
                                style: TextStyle(
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Gap(5.h),
                              Text(
                                dataView.kind.toString(),
                                style: TextStyle(
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ],
                          ),),
                          Center(
                            child:  Text(
                              Functions.prettyDuration(Duration(milliseconds: dataView.trackTimeMillis??0)),
                              style: TextStyle(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),
                  onTap: () {
                    stream.setVideo(dataView.previewUrl??"");
                  },
                );
              }))
        ],
      ),
    );
  }

}